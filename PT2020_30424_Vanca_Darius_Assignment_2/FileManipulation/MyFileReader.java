package FileManipulation;

import java.io.*;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Custom file reader used to read a file based on the given format
 * **/

public class MyFileReader {

    private Scanner scan;

    public MyFileReader(String path){

        File file = new File(path);

        try {
            scan = new Scanner(file);
        }catch(FileNotFoundException e){

            System.out.println("File not Found");
            System.exit(-1);
        }
    }

    public int getNumberOfClients(){
        return scan.nextInt();
    }

    public int getNumberOfQueues(){
        return scan.nextInt();
    }

    public int getMaxSimTime(){
        return scan.nextInt();
    }

    public int[] getInterval() {

        String line = "";

        try {

            line = scan.nextLine();

            if (line.equals(""))
                line = scan.nextLine();

        }catch(NoSuchElementException e){

            System.out.println(e.getMessage());
            System.exit(-1);
        }

        String[] buffer = line.split(",");

        int[] result = new int[2];
        result[0] = Integer.parseInt(buffer[0]);
        result[1] = Integer.parseInt(buffer[1]);

        return result;
    }

}
