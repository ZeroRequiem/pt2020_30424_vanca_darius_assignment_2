package ServersAndClients;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {

    private BlockingQueue<Client> clients;//synchronized data structures to assure thread safety
    private AtomicInteger waitingPeriod;//synchronized data structures to assure thread safety
    private AtomicInteger totalFinishTime;//synchronized data structures to assure thread safety
    private volatile boolean exc = false;//synchronized data structures to assure thread safety, used for the execution of the thread

    /**
     * @param maxClients how many clients can a server hold
     * **/
    public Server(int maxClients){

        clients = new ArrayBlockingQueue<Client>(maxClients);
        waitingPeriod = new AtomicInteger(0);//represents the current waiting period, if a client were to move to the end of the queue how much would it take him to reach the front
        totalFinishTime = new AtomicInteger(0);//sum of finish time of each client, used to compute the average waiting time for a client
    }

    public Queue<Client> getClients(){
        return clients;
    }

    public void add(Client newClient){

        clients.add(newClient);
        totalFinishTime.addAndGet(waitingPeriod.addAndGet(newClient.getServiceTime()));
    }

    public void setExc(boolean val){
        exc = val;
    }

    public int getNrClients(){//how many clients are currently waiting in the server
        return clients.size();
    }

    public int getWaitingPeriod() {
        return waitingPeriod.intValue();
    }

    public AtomicInteger getTotalFinishTime(){ return totalFinishTime;}

    public boolean isEmpty(){//no more clients

        return clients.size() == 0;
    }

    public boolean isRunning(){
        return exc;
    }

    @Override
    public String toString() {//for a pretty print

        String buffer = "";

        for(Client current : clients)
            buffer += current;

        return buffer;
    }

    @Override
    public void run() {

        exc = clients.size() != 0;//make sure that nobody is trying to run a thread when there are no clients

        while(exc){

            Client toBeDeleted = clients.peek();//look at the client in the front

            toBeDeleted.decrementServiceTime();//decrement the service time

            if(toBeDeleted.getServiceTime() == 0)//check if he is done
                clients.poll();//if yes remove him and welcome the next client

            waitingPeriod.decrementAndGet();//each time a time unit passes the actual waiting period for a new client gets decreased

            if(clients.size() == 0) {//no more clients
                exc = false;
                break;//finish
            }

            try{
                Thread.sleep(10000);//used for synchronization such as to not remove the client from the server before the MAIN thread of the simulator prints their status
                                         //the thread will sleep until it will be interrupted by startAllThreads() method from the scheduler
            }catch (InterruptedException e){
                //continue working normally
            }

        }
    }
}
