package ServersAndClients;

public class Client implements Comparable<Client>{

    private int ID;
    private int arrivalTime;//time they arrived at the queue
    private int serviceTime;//how much time it takes to process the client when at front of the queue

    public Client(int ID, int arrivalTime, int serviceTime){

        this.ID = ID;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getID() {
        return ID;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void decrementServiceTime(){
        serviceTime--;
    }

    public boolean serviceIsZero(){
        return serviceTime == 0;
    }//check to see if done processing

    @Override
    public int compareTo(Client o) {//used for the sorting based on arrival time
        return Integer.valueOf(arrivalTime).compareTo(((Client)o).getArrivalTime());

    }

    @Override
    public String toString() {//for a pretty print

        return "(" + ID + "," + arrivalTime + "," + serviceTime + ") ";
    }
}
