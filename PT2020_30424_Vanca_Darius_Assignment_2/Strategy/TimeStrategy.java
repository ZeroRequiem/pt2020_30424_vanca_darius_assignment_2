package Strategy;

import ServersAndClients.*;
import java.util.List;


public class TimeStrategy implements Strategy{

    @Override
    public void addClient(List<Server> servers, Client client) {

        Server minServer = servers.get(0);//select server with minimum waiting period

        for(Server s : servers){

            if(s.getWaitingPeriod() < minServer.getWaitingPeriod())
                minServer = s;
        }

        minServer.add(client);
    }
}
