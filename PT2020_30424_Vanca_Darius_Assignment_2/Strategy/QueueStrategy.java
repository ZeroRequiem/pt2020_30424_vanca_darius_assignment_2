package Strategy;

import ServersAndClients.*;
import java.util.List;

public class QueueStrategy implements Strategy{

    @Override
    public void addClient(List<Server> servers, Client client) {

        Server minServer = servers.get(0);//select server with minimum number of clients currently in the queue

        for(Server s : servers){

            if(s.getNrClients() < minServer.getNrClients())
                minServer = s;
        }

        minServer.add(client);
    }
}
