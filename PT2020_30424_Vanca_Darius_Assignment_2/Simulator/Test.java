package Simulator;

import Simulator.QueuesSimulator;

public class Test {

    public static void main(String[] args) {

        if(args.length != 2){

            System.out.println("Incorrect number of arguments, expected: 2, actually present:" + args.length);
            System.exit(-1);
        }

        QueuesSimulator qs = new QueuesSimulator(args[0], args[1]);
        Thread t = new Thread(qs);
        t.start();

    }
}
