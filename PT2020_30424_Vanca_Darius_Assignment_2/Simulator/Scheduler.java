package Simulator;

import ServersAndClients.Client;
import ServersAndClients.Server;
import Strategy.QueueStrategy;
import Strategy.*;
import Strategy.TimeStrategy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * class responsible with dispatching clients to servers
 * **/
public class Scheduler {

    private List<Server> servers;//list of servers
    private ArrayList<Thread> threads;//list of threads for the servers
    private int maxServers;
    private int maxClientsPerServer;
    private Strategy strategy;//used to select the way we distribute the clients

    public Scheduler(int maxServers, int maxClientsPerServer){

        this.maxServers = maxServers;
        this.maxClientsPerServer = maxClientsPerServer;

        servers = new LinkedList<Server>();
        threads = new ArrayList<Thread>();

        for(int i = 0; i < maxServers; i++)
            servers.add(new Server(maxClientsPerServer));//create all the servers

    }

    public void changeStrategy(SelectionPolicy policy){//select the way you want to distribute your clients

        if(policy == SelectionPolicy.SHORTEST_QUEUE)//based on the number of queues
            strategy = new QueueStrategy();

        if(policy == SelectionPolicy.SHORTEST_TIME)//based on how much they have to wait
            strategy = new TimeStrategy();
    }

    public void dispatchClient(Client client){

        strategy.addClient(servers, client);
    }

    public List<Server> getServers() {
        return servers;
    }

    /**
     * Threads will be created for each server when needed and saved in the list of Threads,
     * when a thread dies it needs to be removed from the list such as to not waste space
     * **/

    public void cleanUpDeadThreads(){

        Thread current;

        for(int i = 0; i < threads.size(); i++){

            current = threads.get(i);

            if(!current.isAlive()) {//if dead

                threads.remove(i);//remove it
                i--;
            }
        }
    }

    /**
     * This method will create threads for servers that currently have clients and are not running any other threads,
     * and start them.
     *
     * Because of the way the program was designed threads could only be in 2 states:
     * sleeping or recently created ( but not started ), since we know for sure this fact
     * we can wake up sleeping threads and start the new ones.
     * **/

    public void startAllThreads(){

        for(Server current : servers) {

            if(!current.isEmpty() && !current.isRunning())//it has clients and is not running any other threads
                threads.add(new Thread(current));//create and save them
        }

        for(Thread current : threads){

            if(current.getState() == Thread.State.TIMED_WAITING)//if sleeping
                current.interrupt();//wake up
            else if(current.getState() == Thread.State.NEW)
                current.start();//it is a recently created thread so start it
        }
    }

    /**
     * @return true if scheduler is running and false otherwise
     *
     * it is considered to be running if there is at least 1 server which is not empty
     * **/

    public boolean isRunning(){

        for(Server currentServer : servers){

            if(!currentServer.isEmpty())
                return true;
        }

        return false;
    }

    /**
     * @return the average waiting time as a sum of finish time for each client ( time spent till they reached the front of the queue + time spent
     * finishing their order ) divided by the total number of clients
     * **/

    public float calculateAvgWaitingTime(){

        float sum = 0f;

        for(Server current : servers){
            sum += current.getTotalFinishTime().floatValue();
        }

        return sum / (float)maxClientsPerServer;
    }
}
