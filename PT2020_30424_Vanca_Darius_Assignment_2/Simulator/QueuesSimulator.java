package Simulator;

import FileManipulation.*;
import ServersAndClients.*;
import Strategy.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class QueuesSimulator implements Runnable{

    private int nrOfClients;
    private int nrOfQueues;
    private int minArrTime;
    private int maxArrTime;
    private int minServiceTime;
    private int maxServiceTime;
    private int maxSimulationTime;
    private MyFileReader mfr;//custom object to read the input file based on the given format
    private BufferedWriter writer;//used to write to the output file
    private ArrayList<Client> clients;//list of clients that are currently waiting

    private Scheduler scheduler;

    /**
     * @param inputFle the path for the input file
     * @param outputFile the path for the output file
     *
     * Constructor for the simulator
     * **/
    public QueuesSimulator(String inputFle, String outputFile){

        mfr= new MyFileReader(inputFle);
        try {
            writer = new BufferedWriter(new FileWriter(outputFile));
        }catch(IOException e){

            System.out.println(e.getMessage());
            System.exit(-1);
        }
        clients = new ArrayList<Client>();

        nrOfClients = mfr.getNumberOfClients();
        nrOfQueues = mfr.getNumberOfQueues();

        maxSimulationTime = mfr.getMaxSimTime();

        int[] result = mfr.getInterval();
        minArrTime = result[0];
        maxArrTime = result[1];

        result = mfr.getInterval();
        minServiceTime = result[0];
        maxServiceTime = result[1];

        scheduler = new Scheduler(nrOfQueues, nrOfClients);
        scheduler.changeStrategy(SelectionPolicy.SHORTEST_TIME);
        generateClients();

    }

    /**
     * @return a random number in the interval of min Arrival time and max Arrival time
     * **/
    public int generateArrivalTime(){

        Random random = new Random();
        return ThreadLocalRandom.current().nextInt(minArrTime, maxArrTime + 1);
    }

    /**
     * @return a random number in the interval of min service time and max service time
     * **/
    public int generateServiceTime(){

        Random random = new Random();
        return ThreadLocalRandom.current().nextInt(minServiceTime, maxServiceTime + 1);
    }

    /**
     * generates N random clients based on the given intervals
     * **/
    public void generateClients(){

        for(int i = 0; i < nrOfClients; i++)
            clients.add(new Client(i, generateArrivalTime(), generateServiceTime()));

        Collections.sort(clients);// sort them based on arrival time
    }

    /**
     * @param currentTime clients will be dispatched to servers based on the currentTime
     * **/
    public void dispatchClients(int currentTime){

        Client currentClient = null;

        for (int i = 0; i < clients.size(); i++) {

            currentClient = clients.get(i);

            if(currentClient.getArrivalTime() != currentTime)//since the clients are sorted based on arrival time if the current one has an arrival time different( smaller )
                break;                                       //than currentTime it is safe to assume that all following clients share the same property

            scheduler.dispatchClient(currentClient);
            clients.remove(currentClient);//delete from waiting clients list
            i--;

        }
    }

    /**
     * @throws IOException for the write() operation
     * used to print the current status of the Queues
     * **/
    public void printStatus() throws IOException{

        Server current = null;

        writer.write("Waiting clients:");

        for(Client currentClient : clients)//display waiting clients
            writer.write(currentClient.toString());

        writer.write("\n");

        for(int i = 0; i < nrOfQueues; i++){//display queues based on status

            current = scheduler.getServers().get(i);

            if(current.isEmpty())
                writer.write("Queue " + i + ": closed\n");
            else
                writer.write("Queue " + i + ":" + current + "\n");
        }

        writer.write("\n");
    }

    /**
     * @throws IOException for the write()
     * **/
    public void terminate(int currentTime) throws IOException{

        writer.write("No more clients, closing all servers...\n");
        writer.write("Average waiting time is:" + scheduler.calculateAvgWaitingTime());
        writer.write("\nSimulation finished at time = " + currentTime);

        System.out.println("SUCCESS");
        writer.close();
    }

    /**
     * @throws IOException for the write()
     * **/
    public void printErrorsAndTerminate()throws IOException{

        System.out.println("FAILURE: Simulation time exceeded!");
        writer.write("\nERROR: Simulation time was exceeded before all the clients could be processed!\n");
        writer.close();
        System.exit(-1);
    }

    @Override
    public void run(){//main Thread which is responsible with simulating the Queues

        int currentTime = 0;

        try {

            while (currentTime < maxSimulationTime) {//continue as long as you didn't reach Max simulation time

                writer.write("Time " + currentTime + "\n");

                dispatchClients(currentTime);

                printStatus();
                scheduler.cleanUpDeadThreads();
                scheduler.startAllThreads();
                currentTime++;//increment currentTime

                try {
                    Thread.sleep(5);//sleep for a given amount of time, used for synchronization
                } catch (InterruptedException e) {}

                if (!scheduler.isRunning() && clients.size() == 0) {//check to see if you are done ( no more waiting clients and all queues are closed )
                    terminate(currentTime);
                    break;
                }
            }

            if(currentTime == maxSimulationTime){//it means that the reason we exited the loop was because the Max simulation time was Exceeded
                printErrorsAndTerminate();//force the program to quit
            }
        }catch (IOException e){
            System.out.println(e.getMessage());
            System.exit(-1);
        }
    }
}
